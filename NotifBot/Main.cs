﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using HtmlAgilityPack;

namespace NotifBot
{
    public partial class Main : Form
    {
        BackgroundWorker bw;
        public struct PInfo
        {
            public string name;
            public string txt;
        }
        public Main()
        {
            InitializeComponent();
            this.bw = new BackgroundWorker();
            this.bw.DoWork += this.bw_DoWork;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        async void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            var worker = sender as BackgroundWorker;
            var key = e.Argument as String; // получаем ключ из аргументов
            try
            {
                var Bot = new Telegram.Bot.TelegramBotClient(key); // инициализируем API
                await Bot.SetWebhookAsync("");
                int offset = 0; // отступ по сообщениям
                while (true)
                {
                    var updates = await Bot.GetUpdatesAsync(offset); // получаем массив обновлений
                    if (DateTime.Now.Minute == 15 && DateTime.Now.Second <= 5)
                    {
                        var list = NotifBot.Load.clients.Keys.ToList();
                        foreach (long item in list)
                        {
                            NotifBot.Load.RefreshT(15, item);
                            var list1 = NotifBot.Load.LoadList(item);
                            foreach (string elem in list1)
                            {
                                await Bot.SendTextMessageAsync(item, "WebSite updated: " + elem);
                            }
                        }
                    }
                    if (DateTime.Now.Minute == 30 && DateTime.Now.Second <= 5)
                    {
                        var list = NotifBot.Load.clients.Keys.ToList();
                        foreach (long item in list)
                        {
                            NotifBot.Load.RefreshT(15, item);
                            NotifBot.Load.RefreshT(30, item);
                            var list1 = NotifBot.Load.LoadList(item);
                            foreach (string elem in list1)
                            {
                                await Bot.SendTextMessageAsync(item, "WebSite updated: " + elem);
                            }
                        }
                    }
                    if (DateTime.Now.Minute == 45 && DateTime.Now.Second <= 5)
                    {
                        var list = NotifBot.Load.clients.Keys.ToList();
                        foreach (long item in list)
                        {
                            NotifBot.Load.RefreshT(15, item);
                            var list1 = NotifBot.Load.LoadList(item);
                            foreach (string elem in list1)
                            {
                                await Bot.SendTextMessageAsync(item, "WebSite updated: " + elem);
                            }
                        }
                    }
                    if (DateTime.Now.Minute == 00 && DateTime.Now.Second <= 5)
                    {
                        var list = NotifBot.Load.clients.Keys.ToList();
                        foreach (long item in list)
                        {
                            NotifBot.Load.Refresh(item);
                            var list1 = NotifBot.Load.LoadList(item);
                            foreach (string elem in list1)
                            {
                                await Bot.SendTextMessageAsync(item, "WebSite updated: " + elem);
                            }
                        }
                    }
                    foreach (var update in updates) // Перебираем все обновления
                    {
                        var message = update.Message;

                        NotifBot.Load.AddClient(message.Chat.Id);
                        
                        if (message.Type == Telegram.Bot.Types.Enums.MessageType.Text)
                        {
                            if (NotifBot.Load.clients[message.Chat.Id].cond)
                            {
                                if (NotifBot.Load.clients[message.Chat.Id].addingsite)
                                {
                                    string lnk = "", name = "", txt = "", t = "";
                                    string mes = message.Text;
                                    bool change = false;
                                    foreach (char symbol in mes)
                                    {
                                        if (change == false)
                                        {
                                            if (symbol == ' ') { change = true; }
                                            else lnk += symbol;
                                        }
                                        if (change == true)
                                        {
                                            t += symbol;
                                        }
                                    }
                                    int time = Convert.ToInt32(t);
                                    if (time != 15 && time != 30 && time != 60)
                                    {
                                        await Bot.SendTextMessageAsync(message.Chat.Id, "Incorrect time", replyToMessageId: message.MessageId);
                                    }
                                    else
                                    {
                                        HttpWebRequest req = null;
                                        try { req = WebRequest.CreateHttp(lnk); }
                                        catch (Exception) { await Bot.SendTextMessageAsync(message.Chat.Id, "Incorrect link", replyToMessageId: message.MessageId); goto m1; }
                                        HttpWebResponse serverResponse;
                                        try { serverResponse = req.GetResponse() as HttpWebResponse; }
                                        catch (Exception) { await Bot.SendTextMessageAsync(message.Chat.Id, "Incorrect link", replyToMessageId: message.MessageId); goto m1;}
                                        txt = Parser.Parse(lnk).txt;
                                        name = Parser.Parse(lnk).name;
                                        await Bot.SendTextMessageAsync(message.Chat.Id, "OK", replyToMessageId: message.MessageId);
                                        NotifBot.Load.Update_L(lnk, time, name, txt, message.Chat.Id);
                                    }
                                    m1:
                                    NotifBot.Load.clients[message.Chat.Id].addingsite = false;
                                }
                                if (message.Text == "/addsite")
                                {
                                    await Bot.SendTextMessageAsync(message.Chat.Id, "Enter divided by space: link and time",
                                           replyToMessageId: message.MessageId);
                                    NotifBot.Load.clients[message.Chat.Id].addingsite = true;
                                }
                                if (message.Text == "/refreshall")
                                {
                                    var list = NotifBot.Load.LoadList(message.Chat.Id);
                                    foreach (string item in list)
                                    { 
                                        await Bot.SendTextMessageAsync(message.Chat.Id, "WebSite updated: " + item);
                                    }
                                }
                            }
                            else
                            {
                                Regex regex = new Regex(@"/login\s\S+\s\S+");
                                if (regex.IsMatch(message.Text))
                                {
                                    string mes = message.Text;
                                    string login = "", password = "";
                                    int change = 0;
                                    foreach (char symbol in mes)
                                    {
                                        if (change == 0)
                                            if (symbol == ' ') { change = 1; continue; }
                                        if (change == 1)
                                        {
                                            if (symbol == ' ') { change = 2; continue; }
                                            else login += symbol;
                                        }
                                        if (change == 2)
                                        {
                                            password += symbol;
                                        }
                                    }
                                    await Bot.SendTextMessageAsync(message.Chat.Id, "Successfully logged!");
                                    if (NotifBot.Load.LogIn(login, password, message.Chat.Id) == true)
                                    {
                                        NotifBot.Load.clients[message.Chat.Id].cond = true;
                                    }
                                    else if (NotifBot.Load.LogIn(login, password, message.Chat.Id) == true) { }
                                }
                            }
                        }
                        offset = update.Id + 1;
                    }
                    
                }
            }
            catch (Telegram.Bot.Exceptions.ApiRequestException ex)
            {
                Console.WriteLine(ex.Message); // если ключ не подошел - пишем об этом в консоль отладки
            }


        }

        private void button1_Click(object sender, EventArgs e)
        {
            var text = textBox1.Text; // получаем содержимое текстового поля txtKey в переменную text
            if (text.Length > 0 && this.bw.IsBusy != true)
            {
                this.bw.RunWorkerAsync(text); // передаем эту переменную в виде аргумента методу bw_DoWork
            }
        }
    }
}
