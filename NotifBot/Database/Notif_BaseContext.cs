﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace NotifBot
{
    public partial class Notif_BaseContext : DbContext
    {
        public Notif_BaseContext()
        {
        }

        public Notif_BaseContext(DbContextOptions<Notif_BaseContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Notification> Notification { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<USLink> USLink { get; set; }
        public virtual DbSet<WebSite> WebSite { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=Notif_Base;Username=postgres;Password=1");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Notification>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name).HasColumnType("character varying");

                entity.Property(e => e.Txt)
                    .IsRequired()
                    .HasColumnType("character varying");

                entity.Property(e => e.Uid).HasColumnName("UId");

                entity.HasOne(d => d.U)
                    .WithMany(p => p.Notification)
                    .HasForeignKey(d => d.Uid)
                    .HasConstraintName("Notification_UId_fkey");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Login)
                    .IsRequired()
                    .HasColumnType("character varying");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnType("character varying");
            });

            modelBuilder.Entity<USLink>(entity =>
            {
                entity.ToTable("U_S_Link");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Sid).HasColumnName("SId");

                entity.Property(e => e.Uid).HasColumnName("UId");

                entity.HasOne(d => d.S)
                    .WithMany(p => p.USLink)
                    .HasForeignKey(d => d.Sid)
                    .HasConstraintName("U_S_Link_SId_fkey");

                entity.HasOne(d => d.U)
                    .WithMany(p => p.USLink)
                    .HasForeignKey(d => d.Uid)
                    .HasConstraintName("U_S_Link_UId_fkey");
            });

            modelBuilder.Entity<WebSite>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Link)
                    .IsRequired()
                    .HasColumnType("character varying");

                entity.Property(e => e.Name).HasColumnType("character varying");

                entity.Property(e => e.Txt).HasColumnType("character varying");
            });
        }
    }
}
