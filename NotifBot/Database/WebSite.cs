﻿using System;
using System.Collections.Generic;

namespace NotifBot
{
    public partial class WebSite
    {
        public WebSite()
        {
            USLink = new HashSet<USLink>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Link { get; set; }
        public string Txt { get; set; }
        public DateTime PrevDate { get; set; }

        public ICollection<USLink> USLink { get; set; }
    }
}
