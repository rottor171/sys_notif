﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;

namespace NotifBot
{
    public static class Parser
    {
        public static Main.PInfo Parse(string link)
        {
            Main.PInfo info = new Main.PInfo();
            HttpWebRequest req = WebRequest.CreateHttp(link);
            HttpWebResponse serverResponse = req.GetResponse() as HttpWebResponse;
            var responseStream = serverResponse.GetResponseStream();
            var streamReader = new StreamReader(responseStream);
            var data = streamReader.ReadToEnd();
            streamReader.Close();
            serverResponse.Close();
            var doc = new HtmlDocument();
            doc.LoadHtml(data);
            info.name = doc.DocumentNode.SelectSingleNode("//title")?.InnerText;

            var pageDescription =
                doc.DocumentNode.SelectNodes("//meta")?
                .FirstOrDefault(n => n.GetAttributeValue("name", "") == "description")?
                .GetAttributeValue("content", null);

            var h1Headers =
                doc.DocumentNode.SelectNodes("//h1")?
                .Select(n => n.InnerText)
                .ToList();

            var txts =
                doc.DocumentNode.SelectNodes("//p")?
                .Select(n => n.InnerText)
                .ToList();

            //var images =
            //    doc.DocumentNode.SelectNodes("//img")?
            //    .Select(n => n.Attributes["src"].Value)
            //    .ToList();

            var links =
                doc.DocumentNode.SelectNodes("//a")?
                .Where(l => l.HasAttributes && l.Attributes["href"] != null)
                .Select(l => l.Attributes["href"].Value)
                .ToList();

            if (h1Headers == null)
                h1Headers = new List<string>();
            //if (images == null)
            //    images = new List<string>();
            if (links == null)
                links = new List<string>();

            string text = "";

            foreach (string item in links)
                if (item.Contains(link))
                    text += item;


            if (text == "")
                foreach (string item in links)
                {
                    string lin = link.Insert(8, "www.");
                    if (item.Contains(lin))
                        text += item;
                    else Console.WriteLine(item + " " + lin + " " + item.Contains(lin));
                }

            //foreach (string item in h1Headers)
            //{
            //    text += item;
            //}

            foreach (string item in txts)
            {
                text += item;
            }

            //foreach (string item in images)
            //{
            //    text += item;
            //}
            text += pageDescription;
            info.txt = text;

            return info;
        }
    }
}
