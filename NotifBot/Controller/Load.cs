﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace NotifBot
{
    public partial class Load
    {
        public class ClientData
        { 
            public int cid = -1;
            public bool cond = false;
            public bool log_proced = false;
            public bool addingsite = false;
        }

        public static Dictionary<long, ClientData> clients = new Dictionary<long, ClientData>();

        public static void AddClient (long chatId)
        {
            if (!clients.ContainsKey(chatId))
            {
                ClientData cl =  new ClientData();
                Notif_BaseContext context = new Notif_BaseContext();
                var c = context.User.FirstOrDefault(uid => uid.ChatId == chatId);
                if (c != null)
                {
                    cl.cond = true;
                    cl.addingsite = false;
                    cl.log_proced = false;
                    cl.cid = c.Id;
                }
                clients.Add(chatId, cl);
            }
        }

        public static string PasHash(string pass)
        {
            byte[] hashp = Encoding.ASCII.GetBytes(pass);
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] hash = md5.ComputeHash(hashp);
            string result = "notif";
            foreach (var b in hash)
            {
                result += b.ToString("x2");
            }
            return result;
        }

        public static bool LogIn(string login, string password, long confid)//Логин
        {
            Notif_BaseContext context = new Notif_BaseContext();
            if (context.User.FirstOrDefault(uid => uid.Login == login && uid.Password == password) == null)
            {
                if (context.User.FirstOrDefault(uid => uid.Login == login) != null || context.User.FirstOrDefault(uid => uid.Password == password) != null)
                    return false;
            }
            var us = context.User.First(uid => uid.Login == login && uid.Password == password); //== PasHash(password)
            us.ChatId = (int)confid;
            context.User.Update(us);
            context.SaveChanges();
            clients[confid].cond = true;
            clients[confid].cid = us.Id;
            clients[confid].log_proced = false;
            clients[confid].addingsite = false;
            return true;
        }

        public static List<string> LoadList(long confid)//Загрузка списка сайтов
        {
            Notif_BaseContext context = new Notif_BaseContext();
            if (context.USLink.FirstOrDefault(uid => uid.Id == clients[confid].cid) == null)
            {
                return new List<string>();
            }
            else
            {
                List<String> list = new List<String>();
                var l1 = context.USLink.Where(uid => uid.Uid == clients[confid].cid);
                List<USLink> l2 = l1.ToList();
                List<WebSite> list2 = new List<WebSite>();
                List<WebSite> list3 = new List<WebSite>();
                foreach (USLink elem in l2)
                {
                    var ll = context.WebSite.FirstOrDefault(sid => sid.Id == elem.Sid);
                    var lll = context.USLink.FirstOrDefault(sid => sid.Sid == ll.Id && sid.Id == elem.Sid);
                    if (ll != null && lll != null)
                    {
                        if (lll.Seen == true) list2.Add(ll);
                        else list3.Add(ll);
                    }
                }
                foreach (WebSite elem in list3)
                {
                    list.Add(elem.Link);
                }
                return list;
            }
        }

        public static void Update_L(string lnk, int t, string name, string txt, long confid)//Добавление нового сайта в список
        {
            Notif_BaseContext context = new Notif_BaseContext();
            var webs = context.WebSite.FirstOrDefault(sid => sid.Link == lnk);
            var ws = context.WebSite.LastOrDefault();
            int newid = 0;
            if (ws != null) newid = ws.Id + 1;
            var newsite = new WebSite
            {
                Id = newid,
                Link = lnk,
                Txt = txt,
                Name = name,
                PrevDate = DateTime.Now
            };
            var x = context.WebSite.FirstOrDefault(sid => sid.Link == lnk);
            if (context.WebSite.FirstOrDefault(sid => sid.Link == lnk) == null) context.Add(newsite);//если сайт с такой ссылкой не существует, добавляем новый
            else if (x != null) { newsite.Id = x.Id; }
            var tusl = context.USLink.FirstOrDefault(sid => sid.Uid == clients[confid].cid && sid.Sid == newsite.Id);
            if (tusl == null)
            {
                var ls = context.USLink.LastOrDefault();
                int newidd = 0;
                if (ls != null) newidd = ls.Id + 1;
                var newlink = new USLink
                {
                    Id = newidd,
                    Uid = clients[confid].cid,
                    Sid = newsite.Id,
                    Time = t,
                    Seen = false,
                    LastDate = DateTime.Now
                };
                context.Add(newlink);
                context.SaveChanges();
            }
        }

        public static void RefreshT(int tim, long confid)//Обновление списка
        {
            Notif_BaseContext context = new Notif_BaseContext();
            if (context.USLink.FirstOrDefault(uid => uid.Id == clients[confid].cid) == null)
            {

            }
            else
            {
                List<String> list = new List<String>();
                var l1 = context.USLink.Where(uid => uid.Uid == clients[confid].cid);
                List<USLink> l2 = l1.ToList();
                List<WebSite> list2 = new List<WebSite>();
                List<WebSite> l3 = new List<WebSite>();
                foreach (USLink elem in l2)
                {
                    var ll = context.WebSite.FirstOrDefault(sid => sid.Id == elem.Sid && elem.Uid == clients[confid].cid);
                    if (ll != null) list2.Add(ll);
                }
                foreach (WebSite item in list2)
                {
                    var ll = context.USLink.FirstOrDefault(sid => sid.Sid == item.Id && sid.Time == tim);
                    if (ll == null) { }
                    else
                    {
                        if ((item.PrevDate - ll.LastDate).Minutes > ll.Time)
                        {
                            var ii = Parser.Parse(item.Link);
                            if (item.Txt == ii.txt) { }
                            else
                            {

                                item.PrevDate = DateTime.Now;
                                item.Txt = ii.txt;
                                ll.Seen = false;
                                ll.LastDate = DateTime.Now;
                                context.WebSite.Update(item);
                                context.USLink.Update(ll);
                                context.SaveChanges();
                            }
                        }
                    }
                }
            }
        }

        public static void Refresh(long confid)//Обновление списка
        {
            Notif_BaseContext context = new Notif_BaseContext();
            if (context.USLink.FirstOrDefault(uid => uid.Uid == clients[confid].cid) == null) { }
            else
            {
                var l1 = context.USLink.Where(uid => uid.Uid == clients[confid].cid);
                List<USLink> l2 = l1.ToList();
                List<WebSite> list2 = new List<WebSite>();
                List<WebSite> l3 = new List<WebSite>();
                foreach (USLink elem in l2)
                {
                    var ll = context.WebSite.FirstOrDefault(sid => sid.Id == elem.Sid);
                    if (ll != null) list2.Add(ll);
                }
                foreach (WebSite item in list2)
                {
                    var ll = context.USLink.FirstOrDefault(sid => sid.Sid == item.Id && sid.Uid == clients[confid].cid);
                    if (ll == null) { }
                    else
                    {
                        if (DateTime.Now.Subtract(item.PrevDate).Minutes > ll.Time)
                        {
                            var ii = Parser.Parse(item.Link);
                            if (item.Txt.Equals(ii.txt) == true) { }
                            else if (item.Txt.Equals(ii.txt) == false)
                            {
                                item.PrevDate = DateTime.Now;
                                item.Txt = ii.txt;
                                ll.Seen = false;
                                ll.LastDate = DateTime.Now;
                                context.WebSite.Update(item);
                                context.USLink.Update(ll);
                                context.SaveChanges();
                            }
                        }
                    }
                }
            }
        }
    }
}
