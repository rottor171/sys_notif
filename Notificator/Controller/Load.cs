﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Notificator
{
    public partial class Load
    {
        public static bool LogIn(string login, string password)//Логин
        {
                Notif_BaseContext context = new Notif_BaseContext();
                if (context.User.FirstOrDefault(uid => uid.Login == login && uid.Password == password) == null)
                {
                    if (context.User.FirstOrDefault(uid => uid.Login == login) != null || context.User.FirstOrDefault(uid => uid.Password == password) != null)
                        return false;
                }
                var i = context.User.FirstOrDefault(uid => uid.Login == login && uid.Password == password);
                Client.SetID(i.Id);
                return true;
            
        }

        public static void LoadList()//Загрузка списка сайтов
        {
            Notif_BaseContext context = new Notif_BaseContext();
            if (context.USLink.FirstOrDefault(uid => uid.Id == Client.GetID()) == null)
            {
                
            }
            else
            {
                List<String> list = new List<String>();
                var l1 = context.USLink.Where(uid => uid.Uid == Client.GetID());
                List<USLink> l2 = l1.ToList();
                List<WebSite> list2 = new List<WebSite>();
                List<WebSite> list3 = new List<WebSite>();
                foreach (USLink elem in l2)
                {
                    var ll = context.WebSite.FirstOrDefault(sid => sid.Id == elem.Sid);
                    //var ii = Parser.Parse(ll.Link);
                    //Program.main.textText(ii.txt);
                    var lll = context.USLink.FirstOrDefault(sid => sid.Sid == ll.Id && sid.Id == elem.Sid);
                    if (ll != null && lll != null)
                    {
                        if (lll.Seen == true) list2.Add(ll);
                        else list3.Add(ll);
                    }
                }
                foreach (WebSite elem in list3)
                {
                    list.Add("* " + elem.Link);
                }
                foreach (WebSite elem in list2)
                {
                    list.Add(elem.Link);
                }
                Program.main.WSList_Load(list);
            }
        }

        public static void LoadNotifList()//Загрузка списка уведомлений
        {
            Notif_BaseContext context = new Notif_BaseContext();
            if (context.Notification.FirstOrDefault(uid => uid.Id == Client.GetID()) == null)
            {

            }
            else
            {
                List<String> list = new List<String>();
                var l1 = context.Notification.Where(uid => uid.Uid == Client.GetID() && (DateTime.Now.Date-uid.PrevDate.Date).TotalDays <= uid.Time);
                List<Notification> l2 = l1.ToList();
                foreach (Notification elem in l2)
                {
                    list.Add(elem.Name);
                    elem.PrevDate = DateTime.Now.AddDays(elem.Time);
                    context.Notification.Update(elem);
                    context.SaveChanges();
                }
                Program.main.NotList_Load(list);
            }

        }
        
        public static void Update_L(string lnk, int t, string name, string txt)//Добавление нового сайта в список
        {
            Notif_BaseContext context = new Notif_BaseContext();
            var webs = context.WebSite.FirstOrDefault(sid => sid.Link == lnk);
            var ws = context.WebSite.LastOrDefault();
            int newid = 0;
            if (ws != null) newid = ws.Id + 1;
            var newsite = new WebSite
            {
                Id = newid,
                Link = lnk,
                Txt = txt,
                Name = name,
                PrevDate = DateTime.Now
            };
            var x = context.WebSite.FirstOrDefault(sid => sid.Link == lnk);
            if (context.WebSite.Count(sid => sid.Link == lnk) == 0) context.WebSite.Add(newsite);//если сайт с такой ссылкой не существует, добавляем новый
            else if (x != null) { newsite.Id = x.Id; }
            var tusl = context.USLink.FirstOrDefault(sid => sid.Uid == Client.GetID() && sid.Sid == newsite.Id);
            if (tusl == null)
            {
                var ls = context.USLink?.Max(sid => sid.Id);
                int newidd = 0;
                if (ls != null) newidd = ls.Value + 1;
                var newlink = new USLink
                {
                    Id = newidd,
                    Uid = Client.GetID(),
                    Sid = newsite.Id,
                    Time = t,
                    Seen = false,
                    LastDate = DateTime.Now
                };
                context.USLink.Add(newlink);
                context.SaveChanges();
            }
            LoadList();
        }

        public static void Update_N(string n, int t, string text, DateTime date)//Добавление нового уведомления в список
        {
            Notif_BaseContext context = new Notif_BaseContext();
            var ws = context.Notification?.Max(sid => sid.Id);
            int newid = 0;
            if (ws != null) newid = ws.Value + 1;
            var newnote = new Notification
            {
                Id = newid,
                Name = n,
                Txt = text,
                PrevDate = date,
                Time = t,
                Uid = Client.GetID()

            };
            context.Add(newnote);
            context.SaveChanges();
            LoadNotifList();
        }

        public static bool RegUs(string login, string password)//регистрация пользователя
        {
                Notif_BaseContext context = new Notif_BaseContext();
                if (context.User.FirstOrDefault(uid => uid.Login == login) != null) return false;
                var us = context.User.LastOrDefault();
                int newid = 0;
                if (us != null) newid = us.Id + 1;
                var newuser = new User
                {
                    Id = newid,
                    Login = login,
                    Password = password
                };
                context.Add(newuser);
                context.SaveChanges();
                return true;
        }

        public static void LoadPrev(string link)//загрузка превью сайта
        {
            Notif_BaseContext context = new Notif_BaseContext();
            if (context.WebSite.FirstOrDefault(sid => sid.Link == link) == null) { }
            else
            {
                var us = context.WebSite.First(sid => sid.Link == link);
                var ls = context.USLink.First(sid => sid.Sid == us.Id);
                ls.Seen = true;
                context.USLink.Update(ls);
                context.SaveChanges();
                Program.prev.LoadPrevPanel(us.Link, us.Name, us.PrevDate, ls.Time);
            }
        }

        public static void DelSite(string lnk)//удаление сайта
        {
            Notif_BaseContext context = new Notif_BaseContext();
            if (context.WebSite.FirstOrDefault(sid => sid.Link == lnk) == null) { }
            else
            {
                var s = context.WebSite.First(sid => sid.Link == lnk);
                var us = context.USLink.First(sid => sid.Sid == s.Id && sid.Uid == Client.GetID());
                context.USLink.Remove(us);
                context.SaveChanges();
                LoadList();
            }
        }

        public static void DelNot(string txt)//удаление уведомления
        {
            Notif_BaseContext context = new Notif_BaseContext();
            if (context.Notification.FirstOrDefault(sid => sid.Name == txt) == null) { }
            else
            {
                var us = context.Notification.First(sid => sid.Name == txt);
                context.Remove(us);
                context.SaveChanges();
                LoadNotifList();
            }
        }

        public static void LoadNotPrev(string n)//загрузка превью уведомления
        {
            Notif_BaseContext context = new Notif_BaseContext();
            if (context.Notification.FirstOrDefault(sid => sid.Name == n) == null) { }
            else
            {
                var us = context.Notification.First(sid => sid.Name == n);
                Program.notprev.LoadNotifPanel(us.Name, us.Txt, us.PrevDate, us.Time);
            }
        }

        public static void RefreshT(int tim)//Обновление списка
        {
            Notif_BaseContext context = new Notif_BaseContext();
            if (context.USLink.FirstOrDefault(uid => uid.Id == Client.GetID()) == null)
            {

            }
            else
            {
                List<String> list = new List<String>();
                var l1 = context.USLink.Where(uid => uid.Uid == Client.GetID());
                List<USLink> l2 = l1.ToList();
                List<WebSite> list2 = new List<WebSite>();
                List<WebSite> l3 = new List<WebSite>();
                foreach (USLink elem in l2)
                {
                    var ll = context.WebSite.FirstOrDefault(sid => sid.Id == elem.Sid && elem.Uid == Client.GetID());
                    if (ll != null) list2.Add(ll);
                }
                foreach (WebSite item in list2)
                {
                    var ll = context.USLink.FirstOrDefault(sid => sid.Sid == item.Id && sid.Time == tim);
                    if (ll == null) { }
                    else
                    {
                        if ((item.PrevDate - ll.LastDate).TotalMinutes > ll.Time)
                        {
                            var ii = Parser.Parse(item.Link);
                            if (item.Txt == ii.txt) { }
                            else
                            {

                                item.PrevDate = DateTime.Now;
                                item.Txt = ii.txt;
                                ll.Seen = false;
                                ll.LastDate = DateTime.Now;
                                context.WebSite.Update(item);
                                context.USLink.Update(ll);
                                context.SaveChanges();
                            }
                        }
                    }
                }
                LoadList();
            }
        }

        public static void Refresh()//Обновление списка
        {
            Notif_BaseContext context = new Notif_BaseContext();
            if (context.USLink.FirstOrDefault(uid => uid.Uid == Client.GetID()) == null){ }
            else
            {
                var l1 = context.USLink.Where(uid => uid.Uid == Client.GetID());
                List<USLink> l2 = l1.ToList();
                List<WebSite> list2 = new List<WebSite>();
                List<WebSite> l3 = new List<WebSite>();
                foreach (USLink elem in l2)
                {
                    var ll = context.WebSite.FirstOrDefault(sid => sid.Id == elem.Sid);
                    if (ll != null) list2.Add(ll);
                }
                foreach (WebSite item in list2)
                {
                    var ll = context.USLink.FirstOrDefault(sid => sid.Sid == item.Id && sid.Uid == Client.GetID());
                    if (ll == null) { }
                    else
                    {
                        //if (DateTime.Now.Subtract(item.PrevDate).TotalMinutes > ll.Time)
                        //{
                            var ii = Parser.Parse(item.Link);
                            if (item.Txt == ii.txt) { Program.main.textText("0"); }
                            else if (item.Txt != ii.txt)
                            {
                                item.PrevDate = DateTime.Now;
                                item.Txt = ii.txt;
                                ll.Seen = false;
                                ll.LastDate = DateTime.Now;
                                Program.main.textText("1");
                                context.WebSite.Update(item);
                                context.USLink.Update(ll);
                                context.SaveChanges();
                            }
                        //}
                    }
                }
                LoadList();
            }
        }
    }
}
