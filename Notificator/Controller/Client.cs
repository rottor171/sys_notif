﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notificator
{
    class Client
    {
        public static int UID;
        public static bool logged;
        Client() { }
        public static void SetID(int id) { UID = id; }
        public static int GetID() { return UID; }
        public static void Logged(bool cond) { logged = cond; }
        public static bool Logged() { return logged; }
    }
}
