﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Notificator
{
    public partial class Site_Preview_Panel : Form
    {
        public Site_Preview_Panel()
        {
            InitializeComponent();
        }

        public void LoadPrevPanel(string lnk, string name, DateTime? date, int time)
        {
            String t = "Refresh every ";
            t += time;
            t += "min.";
            LinkLab.Text = lnk;
            NameLab.Text = name;
            DateLab.Text = date.ToString();
            RefreshLab.Text = t;
        }

        private void OKBut_Click(object sender, EventArgs e)
        {
            Notificator.Load.LoadList();
            Close();
        }

        private void LinkLab_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start(LinkLab.Text);
        }

        private void DelBut_Click(object sender, EventArgs e)
        {
            Notificator.Load.DelSite(LinkLab.Text);
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
