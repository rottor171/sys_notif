﻿namespace Notificator
{
    partial class Main_Panel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.WSList = new System.Windows.Forms.ListBox();
            this.NotList = new System.Windows.Forms.ListBox();
            this.AddWSBut = new System.Windows.Forms.Button();
            this.AddNotBut = new System.Windows.Forms.Button();
            this.LogBut = new System.Windows.Forms.Button();
            this.RefBut = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // WSList
            // 
            this.WSList.FormattingEnabled = true;
            this.WSList.Location = new System.Drawing.Point(537, 110);
            this.WSList.Name = "WSList";
            this.WSList.Size = new System.Drawing.Size(200, 277);
            this.WSList.TabIndex = 0;
            this.WSList.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.WSList_Click);
            // 
            // NotList
            // 
            this.NotList.FormattingEnabled = true;
            this.NotList.Location = new System.Drawing.Point(12, 110);
            this.NotList.Name = "NotList";
            this.NotList.Size = new System.Drawing.Size(206, 277);
            this.NotList.TabIndex = 1;
            this.NotList.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.NotList_Click);
            // 
            // AddWSBut
            // 
            this.AddWSBut.Location = new System.Drawing.Point(226, 12);
            this.AddWSBut.Name = "AddWSBut";
            this.AddWSBut.Size = new System.Drawing.Size(133, 47);
            this.AddWSBut.TabIndex = 2;
            this.AddWSBut.Text = "Add new WebSite";
            this.AddWSBut.UseVisualStyleBackColor = true;
            this.AddWSBut.Click += new System.EventHandler(this.AddWSBut_Click);
            // 
            // AddNotBut
            // 
            this.AddNotBut.Location = new System.Drawing.Point(389, 12);
            this.AddNotBut.Name = "AddNotBut";
            this.AddNotBut.Size = new System.Drawing.Size(127, 47);
            this.AddNotBut.TabIndex = 3;
            this.AddNotBut.Text = "Add New Notification";
            this.AddNotBut.UseVisualStyleBackColor = true;
            this.AddNotBut.Click += new System.EventHandler(this.AddNotBut_Click);
            // 
            // LogBut
            // 
            this.LogBut.Location = new System.Drawing.Point(249, 75);
            this.LogBut.Name = "LogBut";
            this.LogBut.Size = new System.Drawing.Size(110, 47);
            this.LogBut.TabIndex = 4;
            this.LogBut.Text = "Log in";
            this.LogBut.UseVisualStyleBackColor = true;
            this.LogBut.Click += new System.EventHandler(this.LogBut_Click);
            // 
            // RefBut
            // 
            this.RefBut.Location = new System.Drawing.Point(389, 75);
            this.RefBut.Name = "RefBut";
            this.RefBut.Size = new System.Drawing.Size(100, 47);
            this.RefBut.TabIndex = 5;
            this.RefBut.Text = "Refresh";
            this.RefBut.UseVisualStyleBackColor = true;
            this.RefBut.Click += new System.EventHandler(this.RefBut_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(273, 169);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(216, 195);
            this.textBox1.TabIndex = 6;
            this.textBox1.TextChanged += new System.EventHandler(this.WSList_TextChanged);
            // 
            // Main_Panel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(749, 418);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.RefBut);
            this.Controls.Add(this.LogBut);
            this.Controls.Add(this.AddNotBut);
            this.Controls.Add(this.AddWSBut);
            this.Controls.Add(this.NotList);
            this.Controls.Add(this.WSList);
            this.Name = "Main_Panel";
            this.Text = "Main_Panel";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox WSList;
        private System.Windows.Forms.ListBox NotList;
        private System.Windows.Forms.Button AddWSBut;
        private System.Windows.Forms.Button AddNotBut;
        private System.Windows.Forms.Button LogBut;
        private System.Windows.Forms.Button RefBut;
        private System.Windows.Forms.TextBox textBox1;
    }
}