﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Timers;

namespace Notificator
{
    public partial class Main_Panel : Form
    {
        int timtim = 0;

        public Main_Panel()
        {
            InitializeComponent();
            AddWSBut.Hide();
            AddNotBut.Hide();
            RefBut.Hide();
        }

        private void AddWSBut_Click(object sender, EventArgs e)
        {
            Program.addpanel.ShowDialog();
        }

        private void AddNotBut_Click(object sender, EventArgs e)
        {
            Program.addnot.ShowDialog();
        }

        public void textText(String s)
        {
            textBox1.Text += s;
        }

        private void LogBut_Click(object sender, EventArgs e)
        {
            Program.log.ShowDialog();
            if (Client.Logged() == true)
            {
                LogBut.Hide();
                AddWSBut.Show();
                AddNotBut.Show();
                RefBut.Show();
                System.Timers.Timer timer = new System.Timers.Timer();
                timer.Interval = 900000;
                timer.Elapsed += TimerEvent;
                timer.AutoReset = true;
                timer.Enabled = true;
            }
            else Program.log.ShowDialog();
        }

        private static void TimerEvent(Object source, ElapsedEventArgs e)
        {
            if (Program.main.timtim == 0) { Notificator.Load.Refresh(); Program.main.timtim = 1; }
            if (Program.main.timtim == 1) { Notificator.Load.RefreshT(15); Program.main.timtim = 2; }
            if (Program.main.timtim == 2) { Notificator.Load.RefreshT(15); Notificator.Load.RefreshT(30); Program.main.timtim = 3; }
            if (Program.main.timtim == 3) { Notificator.Load.RefreshT(15); Program.main.timtim = 0; }
        }

        private void RefBut_Click(object sender, EventArgs e)
        {
            Notificator.Load.Refresh();
        }

        private void WSList_TextChanged(object sender, EventArgs e)
        {

        }
        

        public void WSList_Load(List<String> list)
        {
                CheckForIllegalCrossThreadCalls = false;
                WSList.BeginUpdate();
                WSList.Items.Clear();
                foreach (string elem in list)
                {
                    WSList.Items.Add(elem);
                }
                WSList.EndUpdate();
        }
        public void NotList_Load(List<String> list)
        {
            foreach (string elem in list)
            {
                NotList.Items.Add(elem);
            }
        }

        private void NotList_Click(object sender, MouseEventArgs e)
        {
            Notificator.Load.LoadNotPrev(NotList.SelectedItem.ToString());
            Program.notprev.ShowDialog();
        }

        private void WSList_Click(object sender, MouseEventArgs e)
        {
            string t = ""; int i = 2;
            if (WSList.SelectedItem.ToString()[0] == '*')
            {
                while (i < WSList.SelectedItem.ToString().Length)
                {
                    t += WSList.SelectedItem.ToString()[i];
                    i++;
                }
                Notificator.Load.LoadPrev(t);
            }
            else Notificator.Load.LoadPrev(WSList.SelectedItem.ToString());
            Program.prev.ShowDialog();
        }
    }
}
