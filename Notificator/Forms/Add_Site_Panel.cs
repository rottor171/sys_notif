﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Notificator
{
    public partial class Add_Site_Panel : Form
    {
        public struct PInfo
        {
            public String txt;
            public string name;
        };
        public Add_Site_Panel()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void AddBut_Click(object sender, EventArgs e)
        {
            HttpWebRequest req = null;
            try
            {
                req = WebRequest.CreateHttp(LinkText.Text);
            }
            catch (Exception)
            {
                LinkText.Text = "Incorrect link";
                return;
            }
            HttpWebResponse serverResponse;
            try
            {
                serverResponse = req.GetResponse() as HttpWebResponse;
            }
            catch(Exception)
            {
                LinkText.Text = "Incorrect link";
                return;
            }
            int time = 0;
            if (RefrechBox.SelectedIndex == 0) time = 15;
            else if (RefrechBox.SelectedIndex == 1) time = 30;
            else if (RefrechBox.SelectedIndex == 2) time = 60;
            else time = 15;
            PInfo info = Parser.Parse(LinkText.Text);
            if(NameText.Text.Length == 0) Notificator.Load.Update_L(LinkText.Text,time, info.name, info.txt);
            else Notificator.Load.Update_L(LinkText.Text, time, NameText.Text, info.txt);
            Close();
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
