﻿namespace Notificator
{
    partial class Add_Site_Panel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LinkText = new System.Windows.Forms.TextBox();
            this.LinkLab = new System.Windows.Forms.Label();
            this.AddBut = new System.Windows.Forms.Button();
            this.RefrechBox = new System.Windows.Forms.CheckedListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.NameLabel = new System.Windows.Forms.Label();
            this.NameText = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // LinkText
            // 
            this.LinkText.Location = new System.Drawing.Point(12, 25);
            this.LinkText.Multiline = true;
            this.LinkText.Name = "LinkText";
            this.LinkText.Size = new System.Drawing.Size(260, 34);
            this.LinkText.TabIndex = 0;
            this.LinkText.Text = "F.e.: https://test.com";
            this.LinkText.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // LinkLab
            // 
            this.LinkLab.AutoSize = true;
            this.LinkLab.Location = new System.Drawing.Point(82, 9);
            this.LinkLab.Name = "LinkLab";
            this.LinkLab.Size = new System.Drawing.Size(110, 13);
            this.LinkLab.TabIndex = 1;
            this.LinkLab.Text = "Enter Web Link Here:";
            this.LinkLab.Click += new System.EventHandler(this.label1_Click);
            // 
            // AddBut
            // 
            this.AddBut.Location = new System.Drawing.Point(85, 167);
            this.AddBut.Name = "AddBut";
            this.AddBut.Size = new System.Drawing.Size(107, 47);
            this.AddBut.TabIndex = 2;
            this.AddBut.Text = "Add";
            this.AddBut.UseVisualStyleBackColor = true;
            this.AddBut.Click += new System.EventHandler(this.AddBut_Click);
            // 
            // RefrechBox
            // 
            this.RefrechBox.FormattingEnabled = true;
            this.RefrechBox.Items.AddRange(new object[] {
            "15 min",
            "30 min",
            "60 min"});
            this.RefrechBox.Location = new System.Drawing.Point(12, 94);
            this.RefrechBox.Name = "RefrechBox";
            this.RefrechBox.Size = new System.Drawing.Size(62, 49);
            this.RefrechBox.TabIndex = 3;
            this.RefrechBox.SelectedIndexChanged += new System.EventHandler(this.checkedListBox1_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Refresh time";
            // 
            // NameLabel
            // 
            this.NameLabel.AutoSize = true;
            this.NameLabel.Location = new System.Drawing.Point(97, 78);
            this.NameLabel.Name = "NameLabel";
            this.NameLabel.Size = new System.Drawing.Size(175, 13);
            this.NameLabel.TabIndex = 5;
            this.NameLabel.Text = "Enter your site name or leave empty";
            this.NameLabel.Click += new System.EventHandler(this.label3_Click);
            // 
            // NameText
            // 
            this.NameText.Location = new System.Drawing.Point(128, 110);
            this.NameText.Name = "NameText";
            this.NameText.Size = new System.Drawing.Size(144, 20);
            this.NameText.TabIndex = 6;
            // 
            // Add_Site_Panel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.NameText);
            this.Controls.Add(this.NameLabel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.RefrechBox);
            this.Controls.Add(this.AddBut);
            this.Controls.Add(this.LinkLab);
            this.Controls.Add(this.LinkText);
            this.Name = "Add_Site_Panel";
            this.Text = "Add_Site_Panel";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox LinkText;
        private System.Windows.Forms.Label LinkLab;
        private System.Windows.Forms.Button AddBut;
        private System.Windows.Forms.CheckedListBox RefrechBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label NameLabel;
        private System.Windows.Forms.TextBox NameText;
    }
}