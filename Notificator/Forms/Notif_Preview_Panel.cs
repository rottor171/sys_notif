﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Notificator
{
    public partial class Notif_Preview_Panel : Form
    {
        public Notif_Preview_Panel()
        {
            InitializeComponent();
        }

        public void LoadNotifPanel(string name, string text, DateTime date, int time)
        {
            String t = "Repeat every ";
            t += time;
            t += "days.";
            NameLabel.Text = name;
            TextLabel.Text = text;
            DateLabel.Text = date.ToLongDateString();
            TimeLabel.Text = t;
        }

        private void OKBut_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void DelBut_Click(object sender, EventArgs e)
        {
            Notificator.Load.DelNot(NameLabel.Text);
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }
    }
}
