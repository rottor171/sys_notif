﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Notificator
{
    public partial class Add_Notif_Panel : Form
    {
        public Add_Notif_Panel()
        {
            InitializeComponent();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void AddBut_Click(object sender, EventArgs e)
        {
            int time = 0;
            if (RepeatBox.SelectedIndex == 0) time = 1;
            else if (RepeatBox.SelectedIndex == 1) time = 7;
            else if (RepeatBox.SelectedIndex == 2) time = 30;
            else if (RepeatBox.SelectedIndex == 3) time = 365;
            else time = 0;
            Notificator.Load.Update_N(NameText.Text, time, TextText.Text, DatePick.Value);
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
