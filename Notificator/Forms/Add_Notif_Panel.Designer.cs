﻿namespace Notificator
{
    partial class Add_Notif_Panel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DatePick = new System.Windows.Forms.DateTimePicker();
            this.DateLabel = new System.Windows.Forms.Label();
            this.RepeatBox = new System.Windows.Forms.CheckedListBox();
            this.RepLabel = new System.Windows.Forms.Label();
            this.AddBut = new System.Windows.Forms.Button();
            this.TextText = new System.Windows.Forms.TextBox();
            this.NameText = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // DatePick
            // 
            this.DatePick.Location = new System.Drawing.Point(35, 61);
            this.DatePick.Name = "DatePick";
            this.DatePick.Size = new System.Drawing.Size(187, 20);
            this.DatePick.TabIndex = 0;
            this.DatePick.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // DateLabel
            // 
            this.DateLabel.AutoSize = true;
            this.DateLabel.Location = new System.Drawing.Point(83, 29);
            this.DateLabel.Name = "DateLabel";
            this.DateLabel.Size = new System.Drawing.Size(110, 13);
            this.DateLabel.TabIndex = 1;
            this.DateLabel.Text = "Choose date and time";
            // 
            // RepeatBox
            // 
            this.RepeatBox.FormattingEnabled = true;
            this.RepeatBox.Items.AddRange(new object[] {
            "Every day",
            "Every week",
            "Every month",
            "Every year"});
            this.RepeatBox.Location = new System.Drawing.Point(12, 104);
            this.RepeatBox.Name = "RepeatBox";
            this.RepeatBox.Size = new System.Drawing.Size(96, 79);
            this.RepeatBox.TabIndex = 2;
            this.RepeatBox.SelectedIndexChanged += new System.EventHandler(this.checkedListBox1_SelectedIndexChanged);
            // 
            // RepLabel
            // 
            this.RepLabel.AutoSize = true;
            this.RepLabel.Location = new System.Drawing.Point(41, 186);
            this.RepLabel.Name = "RepLabel";
            this.RepLabel.Size = new System.Drawing.Size(42, 13);
            this.RepLabel.TabIndex = 3;
            this.RepLabel.Text = "Repeat";
            // 
            // AddBut
            // 
            this.AddBut.Location = new System.Drawing.Point(101, 217);
            this.AddBut.Name = "AddBut";
            this.AddBut.Size = new System.Drawing.Size(75, 23);
            this.AddBut.TabIndex = 4;
            this.AddBut.Text = "Add";
            this.AddBut.UseVisualStyleBackColor = true;
            this.AddBut.Click += new System.EventHandler(this.AddBut_Click);
            // 
            // TextText
            // 
            this.TextText.Location = new System.Drawing.Point(114, 131);
            this.TextText.Multiline = true;
            this.TextText.Name = "TextText";
            this.TextText.Size = new System.Drawing.Size(166, 52);
            this.TextText.TabIndex = 5;
            this.TextText.Text = "Type your notification";
            this.TextText.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // NameText
            // 
            this.NameText.Location = new System.Drawing.Point(115, 104);
            this.NameText.Name = "NameText";
            this.NameText.Size = new System.Drawing.Size(165, 20);
            this.NameText.TabIndex = 6;
            this.NameText.Text = "Type your notif name";
            this.NameText.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // Add_Notif_Panel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.NameText);
            this.Controls.Add(this.TextText);
            this.Controls.Add(this.AddBut);
            this.Controls.Add(this.RepLabel);
            this.Controls.Add(this.RepeatBox);
            this.Controls.Add(this.DateLabel);
            this.Controls.Add(this.DatePick);
            this.Name = "Add_Notif_Panel";
            this.Text = "Add_Notif_Panel";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker DatePick;
        private System.Windows.Forms.Label DateLabel;
        private System.Windows.Forms.CheckedListBox RepeatBox;
        private System.Windows.Forms.Label RepLabel;
        private System.Windows.Forms.Button AddBut;
        private System.Windows.Forms.TextBox TextText;
        private System.Windows.Forms.TextBox NameText;
    }
}