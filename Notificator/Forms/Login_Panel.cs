﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Notificator
{
    public partial class Login_Panel : Form
    {
        //string password;
        public Login_Panel()
        {
            InitializeComponent();
        }

        public new void Close()
        {
            if (Client.Logged() == true) base.Close();
            else { }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void LogBut_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox2.Text != "")
            {
                if (Notificator.Load.LogIn(textBox1.Text, textBox2.Text))
                {
                    Notificator.Load.Refresh();
                    Notificator.Load.LoadNotifList();
                    Client.Logged(true);
                    Close();
                }
                else { textBox1.Text = "Invalid log or pass"; Client.Logged(false); }
            }
            else { textBox1.Text = "Enter log and pass"; Client.Logged(false); }
        }

        private void RegBut_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox2.Text != "")
            {
                if (Notificator.Load.RegUs(textBox1.Text, textBox2.Text))
                {
                    RegBut.Hide();
                }
                else textBox1.Text = "Acc already exist";
            }
            else { textBox1.Text = "Enter log and pass"; Client.Logged(false); }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            
        }
    }
}
