﻿namespace Notificator
{
    partial class Site_Preview_Panel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LinkLab = new System.Windows.Forms.LinkLabel();
            this.NameLab = new System.Windows.Forms.Label();
            this.OKBut = new System.Windows.Forms.Button();
            this.DateLab = new System.Windows.Forms.Label();
            this.RefreshLab = new System.Windows.Forms.Label();
            this.DelBut = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // LinkLab
            // 
            this.LinkLab.AutoSize = true;
            this.LinkLab.Location = new System.Drawing.Point(102, 37);
            this.LinkLab.Name = "LinkLab";
            this.LinkLab.Size = new System.Drawing.Size(83, 13);
            this.LinkLab.TabIndex = 0;
            this.LinkLab.TabStop = true;
            this.LinkLab.Text = "https://test.com";
            this.LinkLab.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLab_LinkClicked);
            // 
            // NameLab
            // 
            this.NameLab.AutoSize = true;
            this.NameLab.Location = new System.Drawing.Point(63, 68);
            this.NameLab.Name = "NameLab";
            this.NameLab.Size = new System.Drawing.Size(49, 13);
            this.NameLab.TabIndex = 1;
            this.NameLab.Text = "Test Site";
            // 
            // OKBut
            // 
            this.OKBut.Location = new System.Drawing.Point(34, 177);
            this.OKBut.Name = "OKBut";
            this.OKBut.Size = new System.Drawing.Size(78, 43);
            this.OKBut.TabIndex = 2;
            this.OKBut.Text = "OK";
            this.OKBut.UseVisualStyleBackColor = true;
            this.OKBut.Click += new System.EventHandler(this.OKBut_Click);
            // 
            // DateLab
            // 
            this.DateLab.AutoSize = true;
            this.DateLab.Location = new System.Drawing.Point(102, 101);
            this.DateLab.Name = "DateLab";
            this.DateLab.Size = new System.Drawing.Size(57, 13);
            this.DateLab.TabIndex = 3;
            this.DateLab.Text = "xx/xx/xxxx";
            this.DateLab.Click += new System.EventHandler(this.label2_Click);
            // 
            // RefreshLab
            // 
            this.RefreshLab.AutoSize = true;
            this.RefreshLab.Location = new System.Drawing.Point(102, 136);
            this.RefreshLab.Name = "RefreshLab";
            this.RefreshLab.Size = new System.Drawing.Size(68, 13);
            this.RefreshLab.TabIndex = 4;
            this.RefreshLab.Text = "Every 15 min";
            // 
            // DelBut
            // 
            this.DelBut.Location = new System.Drawing.Point(176, 177);
            this.DelBut.Name = "DelBut";
            this.DelBut.Size = new System.Drawing.Size(78, 43);
            this.DelBut.TabIndex = 5;
            this.DelBut.Text = "Delete";
            this.DelBut.UseVisualStyleBackColor = true;
            this.DelBut.Click += new System.EventHandler(this.DelBut_Click);
            // 
            // Site_Preview_Panel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.DelBut);
            this.Controls.Add(this.RefreshLab);
            this.Controls.Add(this.DateLab);
            this.Controls.Add(this.OKBut);
            this.Controls.Add(this.NameLab);
            this.Controls.Add(this.LinkLab);
            this.Name = "Site_Preview_Panel";
            this.Text = "Site_Preview_Panel";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.LinkLabel LinkLab;
        private System.Windows.Forms.Label NameLab;
        private System.Windows.Forms.Button OKBut;
        private System.Windows.Forms.Label DateLab;
        private System.Windows.Forms.Label RefreshLab;
        private System.Windows.Forms.Button DelBut;
    }
}