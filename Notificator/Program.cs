﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Notificator
{
    static class Program
    {
        public static Main_Panel main;
        public static Login_Panel log;
        public static Site_Preview_Panel prev;
        public static Add_Site_Panel addpanel;
        public static Add_Notif_Panel addnot;
        public static Notif_Preview_Panel notprev;
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            prev = new Site_Preview_Panel();
            log = new Login_Panel();
            addpanel = new Add_Site_Panel();
            addnot = new Add_Notif_Panel();
            notprev = new Notif_Preview_Panel();
            Application.Run(main = new Main_Panel());
        }
    }
}
